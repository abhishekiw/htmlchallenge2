// function formValidation(){
//     var a = document.forms["validateForm"]["name"];
//     var b = document.forms["validateForm"]["zipCode"];
//     var c = document.forms["validateForm"]["country"];
//     var d = document.forms["validateForm"]["gender"];
//     var e = document.forms["validateForm"]["preferences"];
//     var f = document.forms["validateForm"]["phone"];
//     var g = document.forms["validateForm"]["email"];
//     var h = document.forms["validateForm"]["password"];
//     var i = document.forms["validateForm"]["verifyPassword"];
//     if(a.value == ""){
//         document.getElementById('nameError').innerHTML = "Please enter your name!";
//         return false;
        
//     }
//     if(b.value == "" ){
//        document.getElementById('zipError').innerHTML = "Please enter your 6 character zip code!";
//        return false;
//     }
//     if(c.selectedIndex < 1){
//         document.getElementById('countryError').innerHTML = "Please select your country!";
//     }
//     if(d.checked == ""){
//         document.getElementById('genderError').innerHTML = "Please select your gender!";
//     }
//     if(e.checked == ""){
//         document.getElementById('preferencesError').innerHTML = "Please select your preferences!";
//     }
//     if(f.value == ""){
//         document.getElementById('phoneError').innerHTML = "Please enter your phone number!";
//     }
//     if(g.value == "" || g.value.indexOf("@", 0) < 0 || g.value.indexOf(".", 0) < 0){
//         document.getElementById('emailError').innerHTML = "Please enter your valid email!";
//     }
//     if(h.value == "" || h.value.length > 6 && h.value.length > 8){
//         document.getElementById('passError').innerHTML = "Please enter your password!";
//     }
//     if(i.value == "" || i.value.length > 6 && i.value.length > 8){
//         document.getElementById('vpError').innerHTML = "Please verify your password!";
// }
// }
var v=0;
function vallidate(){
var validated = true;

    if(name()) {
        if(checkZipCode(zipCode)){
            if(checkCountry(this)){
                if(checkPhone(phone)){
                    if(checkEmail()){
                        if(checkPassword()){
                            if(v== 7){
                                reset();
                                alert(document.getElementById('name').value);
                            }
                        }
                    }
                }
            }
        }
    }

    return false;
}


//validation for name
function name(){
    var txtName = document.getElementById("name");
    if (txtName.value == 0 || txtName.value == null) {
        document.getElementById('nameError').innerText = ('Please enter your name!');
        document.getElementById('nameError').style.color = 'red';
        txtName.focus();
        v=0;
        return false;
    }
    else {
        document.getElementById('nameError').innerText = ('');
        v++;
        return true;
    }
}


//validation for zipcode
function checkZipCode(input) {
    if (input.value <= 6) {
        document.getElementById('zipError').innerText = ('Zip code must be of 6 numbers!');
        document.getElementById('zipError').style.color = 'red';
        v=0;
        return false;
    }
    else {
        document.getElementById('zipError').innerText = ('');
        v++;
        return true;
    }

}

//validation for country
function checkCountry() {
    var select=document.getElementById('country')
    if (select.value=='default') {
        document.getElementById('countryError').innerText = ('Please select a  country!');
        document.getElementById('countryError').style.color = 'red';
        v=0;
        return false;
    }
    else {
        document.getElementById('countryError').innerText = '';
        v++;
        return true;
    }
}

//validation for phone
function checkPhone(input){
    if (input.value <= 10) {
        document.getElementById('phoneError').innerText = ('Please enter phone number of 10 digit!');
        document.getElementById('phoneError').style.color = 'red';
        v=0;
        return false;
    }
    else {
        document.getElementById('phoneError').innerText = ('');
        v++;
        return true;
    }
}


//validation for email
function checkEmail(){
    var email = document.getElementById("email");
    if (email.value == 0 || email.value == null) {
        document.getElementById('emailError').innerText = ('Please enter your email!');
        document.getElementById('emailError').style.color = 'red';
        email.focus();
        v=0;
        return false;
    }
    else {
        document.getElementById('emailError').innerText = ('');
        v++;
        return true;
    }
}


//validation for password
function checkPassword(){
    var pass = document.getElementById("password");
    if (pass.value == 0 || pass.value == null) {
        document.getElementById('passError').innerText = ('Please enter your password!');
        document.getElementById('passError').style.color = 'red';
        pass.focus();
        v=0;
        return false;
    }
    else {
        document.getElementById('passError').innerText = ('');
        v++;
        return true;
    }
}

//validation for verifying password
var check = function() {
    if (document.getElementById('password').value ==
      document.getElementById('verifyPassword').value) {
      document.getElementById('vpError').style.color = 'green';
      document.getElementById('vpError').innerHTML = 'Password Matched';
      v=0;
    } 
    else {
      document.getElementById('vpError').style.color = 'red';
      document.getElementById('vpError').innerHTML = 'Password not matched';
      v++;
    }
  }

//clear data
  function reset()
  {
      document.getElementsByClassName("").innerHTML = 'validationError';
      document.getElementById("nameError").innerHTML = "";
      document.getElementById("zipError").innerHTML = "";
      document.getElementById("countryError").innerHTML = "";
      document.getElementById("genderError").innerHTML = "";
      document.getElementById("preferencesError").innerHTML = "";
      document.getElementById("phoneError").innerHTML = "";
      document.getElementById("emailError").innerHTML = "";
      document.getElementById("passError").innerHTML = "";
      document.getElementById("vpError").innerHTML = "";
  
      document.getElementById("name").value="";
      document.getElementById("address").value="";
      document.getElementById("zipCode").value="";
      document.getElementById("country").value="";
      
       document.forms["validateForm"]["gender"].value="";
      document.forms["validateForm"]["preferences"].value="";
      
      document.getElementById("phone").value="";
      document.getElementById("email").value="";
      document.getElementById("password").value="";
  document.getElementById("verifyPassword").value="";
      
  
  }

//display data in alert box
// window.onload = function(){
//     document.getElementById('send').onclick = function(e){
//     var n = document.getElementById('name');
//     var o = document.getElementById('address');
//     var p = document.getElementById('zipCode');
//     var q = document.getElementById('country');
//     var r = document.getElementById('gender');
//     var s = document.getElementById('preferences');
//     var t = document.getElementById('phone');
//     var u = document.getElementById('email');
//     var v = document.getElementById('password');
//     var w = document.getElementById('verifyPassword');
//         if(n.value != "", o.value != "",p.value != "",q.value != "",r.value != "",s.value != "",t.value != "",u.value != "",v.value != "",w.value != ""){
//             alert("Name: " + document.getElementById("name").value + '\n' +
//                 "Address: " + document.getElementById("address").value + '\n' +
//                 "Zip Code: " + document.getElementById("zipCode").value + '\n' +
//                 "Country: " + document.getElementById("country").value + '\n' +
//                 "Gender: " + document.getElementById("gender").value + '\n' +
//                 "Preferences: " + document.getElementById("preferences").value + '\n' +
//                 "Phone: " + document.getElementById("phone").value + '\n' +
//                 "Email: " + document.getElementById("email").value + '\n' +
//                 "Password: " + document.getElementById("password").value + '\n' +
//                 "Verify Password: " + document.getElementById("verifyPassword").value + '\n');
//         return false;
//         }
//     }
// }